import React from 'react';

function Boton(props) {
    let textoBoton =  (props.estado) ? "Apagar" : "Encender" ;
    return (
        <button onClick={props.apretar} >{textoBoton}</button>
    );
}

export default Boton;
