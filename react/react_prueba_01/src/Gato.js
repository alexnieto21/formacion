import React from "react";

function Gato200(){
  return(
      <img src="http://placekitten.com/200/200" />
  );
}

function Gato300(){
    return(
        <img src="http://placekitten.com/300/300" />
    );
}

export {Gato200, Gato300}
