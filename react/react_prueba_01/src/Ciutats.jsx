import React from "react";
import { CIUTATS } from "./datos.js";
import { Table } from "reactstrap";

class Ciutats extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let ciudadesLi = CIUTATS.filter(el => (el.toLowerCase())[0] === 's')
            .map((el, indice) => <li key={indice}>{el}</li>);

        let filas = CIUTATS.map((el, index) => <tr key={index}><td>{index}</td><td>{el}</td></tr>);

        return (
            <>
                <h1>Ciutats</h1>
                <Table >
                    <thead>
                        <th>num</th>
                        <th>Ciutat</th>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
                {/* <ul>
                    {ciudadesLi}
                </ul> */}
            </>
        );
    }
}

export default Ciutats;