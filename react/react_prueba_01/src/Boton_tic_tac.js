import React from "react";
import "./css/boton_tic_tac.css";

class BotonTicTac extends React.Component{
    
    constructor(props){
        super(props);

        this.state={
            iconoVisible: false,
            valor: ""
        }

        this.clicar = this.clicar.bind(this);

    }   

    clicar(){
        if(!this.state.iconoVisible && !this.props.winner){
            this.setState({iconoVisible: true, valor: this.props.turno});
            this.props.cambiaTurno(this.props.orden, this.props.turno);
        }
    }

    render(){

        let circulo = "fa fa-circle-o icono";
        let cruz = "fa fa-times icono";
        let clasesIcono = (this.state.valor==="x")? cruz : circulo;

        if(!this.state.iconoVisible){
            clasesIcono = clasesIcono + " oculto";
        }

        return(
            <div className="boton-tic-tac" onClick={this.clicar}>
                <i className={clasesIcono} aria-hidden="true"></i>
            </div>
        );
    }
}

export default BotonTicTac;