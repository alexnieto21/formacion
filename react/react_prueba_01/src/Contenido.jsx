import React from 'react';
import Bola from './Bola';

function TituloRojo(props){
    return (
        <h1 style={{color:"red"}}>{props.children}</h1>
    );
}

function Contenido(props){

    return(
        <>
            <TituloRojo>Capitulo 1</TituloRojo>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
            Explicabo quis aspernatur quibusdam voluptatem cupiditate sit error rem hic 
            architecto quod ut velit, sequi modi. Laboriosam velit eius hic provident exercitationem.</p>        

            <h1>Capitulo 2</h1>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
            Explicabo quis aspernatur quibusdam voluptatem cupiditate sit error rem hic 
            architecto quod ut velit, sequi modi. Laboriosam velit eius hic provident exercitationem.</p>

            <Bola />
        </>
    );

}

export default Contenido;