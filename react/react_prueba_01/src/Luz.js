import React from 'react';

import Bombilla from './Bombilla';
import Boton from './Boton';


export default class Luz extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            encendido: true,
        }
        this.pulsar = this.pulsar.bind(this);
    }
    
    pulsar(){
        this.setState({
            encendido: !this.state.encendido
        })
    }

    render(){
        
        return (
            <div className="caja-luz">
                <Bombilla estado={this.state.encendido} />
                <Boton apretar={this.pulsar} 
                       estado={this.state.encendido} />
            </div>
        );
    }

}
