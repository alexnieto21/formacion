import React from 'react';

const Bombilla = (props) => {

    let imgOn = 'https://toppng.com/uploads/preview/light-bulb-on-off-png-11553940319kdxsp3rf0i.png';
    let imgOff = 'https://toppng.com/uploads/preview/light-bulb-on-off-png-11553940208oq66nq8jew.png';

    let imagen = (props.estado) ? imgOn : imgOff;
    return <img src={imagen} width="200px" />
}

export default Bombilla;
