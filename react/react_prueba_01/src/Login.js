import React from "react";
import { Button, Form, FormGroup, Label, Input, FormText, Container } from 'reactstrap';

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            errorPassword: false
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.state.password.length<8){
            this.setState({errorPassword:true});
        }
    }

    render() {

        let mensajeErrorPassword = null;

        if(this.state.errorPassword){
            mensajeErrorPassword=<small className="text-danger">Error! Password incorrecto</small>
        }

        return (
            <>
                <Container className="jumbotron">
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="campoEmail" className="mr-sm-2">Email</Label>
                            <Input onChange={this.handleInputChange} type="email" name="email" id="campoEmail" placeholder="Entra email" />
                        </FormGroup>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="campoPassword" className="mr-sm-2">Password</Label>
                            <Input onChange={this.handleInputChange} type="password" name="password" id="campoPassword" placeholder="Entra password" />
                            {mensajeErrorPassword}
                        </FormGroup>
                        <Button>Submit</Button>
                    </Form>
                    <h3>{this.state.password}</h3>
                    
                </Container>
            </>
        );
    }
}

export default Login;