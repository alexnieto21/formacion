import React from "react";
import "./css/app.css";
import { Gato200, Gato300 } from "./Gato";
import Cabecera from "./Cabecera";
import Contenido from "./Contenido";
import Logo from "./Logo";
import Luz from "./Luz";
import { Container, Alert, Row, Col } from "reactstrap";
import BotonTicTac from "./Boton_tic_tac";
import Ciutats from "./Ciutats";
import Login from "./Login";

function App(){

  let titulo = "Mi super app"

  return(
    <>
      {/* <h1>Hola!</h1>
      <Cabecera titulo={titulo} color="red"/>
      <Contenido />
      <Gato200/>
      <Gato300/>
      <Logo />
      <Luz />
      <Ciutats /> */}
      <Login />
    </>
  );
}

// class App extends React.Component {

//   constructor(props){
//     super(props);

//     this.state = {
//       turno: "x",
//       mapa: [0,0,0, 0,0,0, 0,0,0],
//       winner: false
//     }

//     this.cambiaTurno = this.cambiaTurno.bind(this);
//     this.reiniciar = this.reiniciar.bind(this);

//   }

//   analizaMapa(){

//     console.log(this.state.mapa);

//     let casillasWinners = [
//       [1,2,3],
//       [4,5,6],
//       [7,8,9],
//       [7,5,3],
//       [9,5,1],
//       [7,4,1],
//       [8,5,2],
//       [9,6,3]
//     ]

//     //Como cuando ya ha llegado a esta funcion, ya se ha cambiado de turno, se tiene que coger el turno anterior
//     //Si 'o'es igual 1 será 2
//     //Si 'x' es igual 2 será 1
//     let turno = (this.state.turno=="x")? 1: 2; 

//     for(let cadena of casillasWinners){
//       let contador = 0;
//       for(let numero of cadena){
        
//         if(this.state.mapa[numero-1]==turno){
//           contador++;
//         }

//       }
      
//       if(contador==3){
//         this.setState({winner:true});
//         break;
//       }

//     }
//   }

//   cambiaTurno(boton, jugada){
//     let nuevoTurno = (this.state.turno==="x") ? "o" : "x";
//     //console.log(boton, jugada);
//     let posicion = boton-1;
//     jugada = (jugada==="o") ? 1 : 2;
//     let nuevoMapa = [...this.state.mapa];
//     nuevoMapa[posicion]=jugada;
//     //Al ser el setState asincrono, es mejor poner la funcion analizaMapa 
//     //despues de que se hayan hecho los cambios, si se pone debajo es posible que se ejecute antes el
//     //analizaMapa que el setState
//     this.setState({mapa: nuevoMapa, turno: nuevoTurno}, this.analizaMapa);
//   }

//   reiniciar(){

//   }

//   render() {
//     return (
//       <Container>
//         <h1 className="text-center">3 en raya</h1>
//         <div className="hola">
//         <Row>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={1} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={2} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={3} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//         </Row>
//         <Row>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={4} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={5} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={6} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//         </Row>
//         <Row>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={7} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={8} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//           <Col>
//             <BotonTicTac winner={this.state.winner} orden={9} cambiaTurno={this.cambiaTurno} turno={this.state.turno} />
//           </Col>
//         </Row>
//         </div>
        

//         <div className={(!this.state.winner)? "d-none": "d-block mt-4"}>
//           <h3 className="text-center">{(this.state.turno=="x")? "Ha ganado Jugador 2": "Ha ganado Jugador 1"}</h3>
//         </div>
//         <div className="text-center mt-4">
//           <button className="btn btn-primary btn-lg" onClick={this.reiniciar}>Reiniciar</button>
//         </div>

//       </Container>
//     );
//   }
// }

export default App;