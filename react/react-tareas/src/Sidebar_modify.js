import React from "react";
import { Input, Button } from "reactstrap";
import styled from 'styled-components';

/*
Hacer que el boton de nueva tarea funcione
Mejorar presentación
Hacer que el input de color muestre selector de colores
...
*/

const Side = styled.div`
  box-shadow: 2px 2px 3px 0px #cacaca;
  background-color: rgba(208, 208, 208, 0.4);
  height: 80vh;
  padding: 30px 20px 0 20px;
  width: 100%;
`;


class SideBar_modify extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            texto: props.textoM,
            color: props.colorM,
            prioridad: props.prioridadM,
            errorTexto: false
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.cambiarTarea = this.cambiarTarea.bind(this);
        this.cancelar = this.cancelar.bind(this);
      }
    
      handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
      }
    
      cambiarTarea(){
        
        if(this.state.texto.trim()===""){
          this.setState({errorTexto: true});
        }else{
          this.setState({errorTexto: false, texto: '', color: '#ffffff', prioridad: 'Normal'});
          this.props.cambiarTarea(this.state.texto, this.state.color, this.state.prioridad)
        }
      }
      
      cancelar(){
        this.props.cancelar();
      }

render(){

    let error = (this.state.errorTexto)? "La tarea tiene que tener un titulo": "";

     return (
        <Side>
          <h4>Modificar Tarea</h4>
          Tarea
          <Input type="text" name="texto" onChange={this.handleInputChange} value={this.state.texto}/>
          <small className="text-danger">{error}</small>
          <br />
          Color
          <Input type="color" name="color" onChange={this.handleInputChange} value={this.state.color}/>
          <br />
          Prioridad
          <Input type="select" name="prioridad" onChange={this.handleInputChange} value={this.state.prioridad}>
            <option value="Normal">Normal</option>
            <option value="Secundaria">Secundaria</option>
            <option value="Urgente">Urgente</option>
            <option value="Importante">Importante</option>
          </Input>
          <br />
          <Button onClick = {this.cambiarTarea} className="btn btn-success mr-2"><i className="fa fa-plus" aria-hidden="true"></i> Guardar</Button>
          <Button onClick = {this.cancelar} className="btn btn-danger"><i className="fa fa-ban" aria-hidden="true"></i> Cancelar</Button>
        </Side>
    )
}
   
}

export default SideBar_modify;
