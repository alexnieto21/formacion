import React from "react";
import {Container} from 'reactstrap';
import styled from 'styled-components';
 
const Head = styled.div`
    background-image: linear-gradient(rgba(0, 27, 90, 0.3215686274509804),rgba(255, 255, 255, 0));
    height: 12vh;
    padding-top: 10px;
    color: white;
`;

function Header(){

    return (
        <Head>

            <Container>        
                <h1><i className="fa fa-address-book-o " aria-hidden="true"></i> Tareas</h1>
            </Container>

        </Head>

    )
}

export default Header;
