import React from "react";
import { Button, Row, Col } from "reactstrap";
import styled from 'styled-components';
import Filtro from './Filtro';

const Cont = styled.div`
  min-height: 400px;
  padding: 0 20px;
`;

const Tarea = styled.div`
    box-shadow: 2px 2px 6px 0px #d8d8d8;
    margin: 10px;
    padding: 10px;
    background-color: ${el => el.fondo};
`;

const Titulo = styled.h4`
    padding: 10px 0 10px 10px;
    background-color: rgba(208,208,208,0.4);
    box-shadow: 2px 2px 3px 0px #cacaca;
    text-align: center;
    letter-spacing: 10px;
`;

/*
  mejorar presentación poniendo color de fondo en cada tarea,
 hacer que el botón eliminaTarea funcione correctamente y reemplazarlo por icono "fa-trash"
*/

class Content extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            textoFiltro: "",
            prioridadFiltro: ""
        }

        this.filtrar = this.filtrar.bind(this);
    }

    filtrar(texto, prioridad) {
        this.setState({ textoFiltro: texto, prioridadFiltro: prioridad });
    }

    render() {
        let tar = JSON.parse(JSON.stringify(this.props.tareas));

        tar = tar.map(el => {
            let mostrar = false;
            if (el.texto.includes(this.state.textoFiltro)) {
                if (this.state.prioridadFiltro !== "") {
                    if (this.state.prioridadFiltro === el.prioridad) {
                        mostrar = true;
                    }
                } else {
                    mostrar = true;
                }
            }

            if (mostrar) {
                return (<Tarea key={el.id} fondo={el.color}>
                    <Row>
                        <Col><p>{el.texto}</p></Col>
                        <Col><p className="text-right text-secondary mr-4">{el.prioridad}</p></Col>
                    </Row>

                    <Button onClick={() => this.props.modificaTarea(el)} className="btn btn-info"><i className="fa fa-pencil-square-o" aria-hidden="true"></i> Modificar</Button>
                    <Button onClick={() => this.props.eliminaTarea(el.id)} className="ml-2 btn btn-danger"><i className="fa fa-trash-o" aria-hidden="true"></i></Button>
                </Tarea>)
            }

        }
        )

        return (

            <Cont>
                <Titulo>TAREAS</Titulo>
                <Filtro filtro={this.filtrar} />
                {tar}
            </Cont>

        );
    }

}

export default Content;
