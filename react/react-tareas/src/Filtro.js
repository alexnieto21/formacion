import React from "react";
import { Input, Row, Label, Col, FormGroup, Form } from "reactstrap";

class Filtro extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            texto: '',
            prioridad: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.filtrar = this.filtrar.bind(this);
    }

    filtrar() {
        this.props.filtro(this.state.texto, this.state.prioridad);
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        }, this.filtrar);
    }

    render() {

        let inputs = {
            width: "250px",
        }

        return (
            <>
                <Form inline className="d-flex justify-content-end">
                    <FormGroup className="mb-2 mr-sm-4 mb-sm-0">
                        <Input type="select" name="prioridad" style={inputs} onChange={this.handleInputChange} value={this.state.prioridad} >
                            <option value=""></option>
                            <option value="Normal">Normal</option>
                            <option value="Secundaria">Secundaria</option>
                            <option value="Urgente">Urgente</option>
                            <option value="Importante">Importante</option>
                        </Input>
                    </FormGroup>
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="texto" className="mr-sm-2"><i className="fa fa-search" aria-hidden="true"></i></Label>
                        <Input type="text" name="texto" style={inputs} onChange={this.handleInputChange} value={this.state.texto} />
                    </FormGroup>
                </Form>
            </>
        );
    }

}

export default Filtro;