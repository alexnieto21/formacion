import React from "react";
import { Container, Row, Col } from "reactstrap";
import './css/app.css';

import Header from './Header';
import SideBar from './Sidebar';
import SideBar_modify from "./Sidebar_modify";
import Content from './Content';

const TAREAS_DEMO = [
  {
    id: 1,
    texto: "Comprar mascarilla",
    color: "red",
    prioridad: "normal"
  },
  {
    id: 2,
    texto: "Hacer ejercicio TAREAS",
    color: "green",
    prioridad: "normal"
  },
  {
    id: 3,
    texto: "Jugar GTA",
    color: "orange",
    prioridad: "normal"
  },

];

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      tareas: (localStorage.getItem('tareas') !== null) ? JSON.parse(localStorage.getItem('tareas')) : TAREAS_DEMO,
      modificando: false,
      idM: "",
      textoM: "",
      colorM: "",
      prioridadM: ""
    };

    this.eliminaTarea = this.eliminaTarea.bind(this)
    this.nuevaTarea = this.nuevaTarea.bind(this)
    this.modificaTarea = this.modificaTarea.bind(this);
    this.cambiarTarea = this.cambiarTarea.bind(this);
    this.cancelar = this.cancelar.bind(this);
  }

  asignarId() {
    let id = this.state.tareas.length;
    let existe = false;
    do {
      existe = false;
      this.state.tareas.map(el => {
        if (el.id === id) {
          existe = true;
          id++;
        }
      });
    } while (existe);

    return id;

  }

  guardarTareas() {
    localStorage.setItem('tareas', JSON.stringify(this.state.tareas));
  }

  nuevaTarea(texto, color, prioridad) {
    let tarea = { id: this.asignarId(), texto: texto, color: color, prioridad: prioridad }
    let nuevaLista = [...this.state.tareas];
    nuevaLista.push(tarea);

    this.setState({ tareas: nuevaLista }, this.guardarTareas);

  }

  eliminaTarea(id) {
    //Con estas lineas te aseguras que si el objeto contiene objetos, todo queda copiado, y no dará problemas
    // let tar = JSON.parse(JSON.stringify(this.state.tareas));
    // tar = tar.filter(el=>el.id!==id);
    // this.setState({tareas: tar}, this.guardarTareas);

    this.setState({ tareas: this.state.tareas.filter(el => el.id !== id) }, this.guardarTareas);
  }

  modificaTarea(tarea) {
    this.setState({modificando: true, idM: tarea.id, textoM: tarea.texto, colorM: tarea.color, prioridadM: tarea.prioridad})
  }

  cambiarTarea(texto, color, prioridad){
    let tarea = JSON.parse(JSON.stringify(this.state.tareas));

    tarea = tarea.map(el=> {
          if(el.id===this.state.idM){
            el.texto = texto;
            el.color = color;
            el.prioridad = prioridad;
          }
          return el;
    });

    this.setState({tareas: tarea, modificando: false});

  }

  cancelar(){
    this.setState({modificando: false});
  }

  render() {
    return (
      <>
        <Header />
        <Container className="mt-4">
          <Row>
            {
              (this.state.modificando) ?
                (
                  <Col xs="3" className="p-0">
                    <SideBar_modify cambiarTarea={this.cambiarTarea} cancelar={this.cancelar} textoM={this.state.textoM} colorM={this.state.colorM} prioridadM={this.state.prioridadM} />
                  </Col>
                )
                : (
                  <Col xs="3" className="p-0">
                    <SideBar nuevaTarea={this.nuevaTarea} />
                  </Col>
                )
            }
            <Col className="p-0">
              <Content tareas={this.state.tareas} eliminaTarea={this.eliminaTarea} modificaTarea={this.modificaTarea}/>
            </Col>
          </Row>
        </Container>
      </>
    );

  }

}

export default App;
