import React from "react";

function Marco(props){

    let estilos = {
        backgroundColor: props.fondo,
        borderWidth: props.borde,
        borderColor: props.color,
        borderStyle: "solid",
        padding: "10px",
        display: "block"
    }

    return (
        <>
            <img src={props.src} style={estilos} />
        </>
    );
}

export default Marco;