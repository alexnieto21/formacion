import React from "react";
import "./css/mosca.css";

function Mosca(props){

    return(
        <>
            <i className="fa fa-user mosca" style={{color: props.color}} aria-hidden="true"></i>
        </>
    );
}

export default Mosca;