import React from "react";
import Titulo from "./Titulo";
import "./css/app.css";
import BolaX from "./BolaX";
import CuadradoB from "./CuadradoB";
import Mosca from "./Mosca";
import Capital from "./Capital";
import Gato from "./Gato";
import BolaBingo from "./BolaBingo";
import FotoBolaX from "./FotoBolaX";
import Marco from "./Marco";
import Flipicon from "./Flipicon";
/*import HolaMundo from "./Hola";
import Bola from "./Bola";
import Cuadrado from "./Cuadrado";
import Separador from "./Separador";*/



export default () => (
  <>

    {/*Ejercicios sin props*/}
    
    {/*<HolaMundo />
    <Bola />
    <Cuadrado />
    <Separador />*/}

    {/*Ejercicios con props*/}

    <Titulo texto="Hola React"/>
    <BolaX talla="80px" margen="10px" fondo="#ff0000"/>
    <CuadradoB talla="70px" margen="8px" grosor="5px" color="red" />
    <Mosca color="blue" />
    <Capital nom="barcelona" />
    <Gato ancho="200" alto="200" nombre="Garfield"/>
    <BolaBingo num="22" />
    <FotoBolaX src="http://placekitten.com/200/200" talla="200"/>
    <Marco src="http://placekitten.com/200/300" borde="10" color="brown" fondo="beige" />
    <Flipicon icon1="fa-thumbs-up" icon2="fa-thumbs-down" />
  </>
);
