import React from "react";
import "./css/bolax.css";

function BolaX(props){
    
    let estilos = {
        height: props.talla,
        width: props.talla,
        margin: props.margen,
        backgroundColor: props.fondo
    }

    return(
        <>
            <div className="bolax" style={estilos}></div>
        </>
    );
}

export default BolaX;