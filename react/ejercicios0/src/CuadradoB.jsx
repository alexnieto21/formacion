import React from "react";

function CuadradoB(props){

    let estilos = {
        height: props.talla,
        width: props.talla,
        margin: props.margen,
        borderColor: props.color,
        borderWidth: props.grosor,
        borderStyle: "solid"
    }

    return(

        <>
            <div className="cuadradob" style={estilos}></div>
        </>

    );
}

export default CuadradoB;