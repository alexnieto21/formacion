import React from "react";

function FotoBolaX(props){

    let estilos = {
        width: props.talla,
        height: props.talla,
        borderRadius: props.talla/2
    }

    return(
        <>
            <img src={props.src} style={estilos}/>
        </>
    );
}

export default FotoBolaX;