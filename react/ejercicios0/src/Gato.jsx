import React from "react";
import "./css/gato.css";

function Gato(props) {

    return (
        <>
            <div className="gato">
                <img src={"http://placekitten.com/" + props.ancho + "/" + props.alto} />
                <p className="text-center mb-0">{props.nombre}</p>
            </div>
        </>
    );
}

export default Gato;