import React from "react";

class Flipicon extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            icono: this.props.icon1
        }

        this.cambia = this.cambia.bind(this);
    }

    cambia() {
        let nuevoState = {
            icono: this.props.icon2
        }
        this.setState(nuevoState);
    }


    render() {
        return (
            <div>
                <i className={"fa " + this.state.icono} style={{fontSize:"50px"}}></i>
                <br/>
                <button onClick={this.cambia}>Cambia</button>
            </div>
        );
        
    }
}

export default Flipicon;