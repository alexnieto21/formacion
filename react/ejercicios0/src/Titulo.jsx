import React from "react";
import "./css/titulo.css";

function Titulo(props){
    return (
        <>
        
            <h1>{props.texto}</h1>

        </>
    );
}

export default Titulo;