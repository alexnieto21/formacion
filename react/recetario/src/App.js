import React from "react";
import Navegacion from "./Navegacion";
import Inicio from "./Inicio";
import Productos from "./Productos";
import Recetas from "./Recetas";
import EditaReceta from "./EditaReceta";
import Error404 from "./Error404.js";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Container } from "reactstrap";

export default () => (
  <>
    <BrowserRouter>
      <Navegacion />
      
      <Container>
        <Switch>
          <Route exact path="/" component={Inicio} />
          <Route path="/productos" component={Productos} />
          <Route path="/recetas" component={Recetas} />
          <Route path="/EditaReceta/:idReceta" component={EditaReceta} />
          <Route component={Error404} />
        </Switch>

      </Container>
    </BrowserRouter>



  </>
);
