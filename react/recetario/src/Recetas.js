import React from "react";
import Receta from "./modelos/Receta";
import { Table, Button, Input, Label } from 'reactstrap';
import { Link } from 'react-router-dom';

class Recetas extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            recetas: Receta.getRecetas(),
            nombre: '',
            descripcion: ''
            // comensales: '',
            // dificultad: '',
            // minutos: '',
        }
        this.recetaNueva = this.recetaNueva.bind(this);
        this.borra = this.borra.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    recetaNueva() {

        let nuevaReceta = {
            nombre: this.state.nombre,
            descripcion: this.state.descripcion
        }

        Receta.addReceta(nuevaReceta);
        this.setState({ recetas: Receta.getRecetas(), nombre: '', descripcion: ''});


    }

    borra(id) {
        Receta.eliminaReceta(id * 1);
        this.setState({ recetas: Receta.getRecetas() });
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    // getTimeFromMins(mins) {
    //     let moment = require('moment');

    //     // do not include the first validation check if you want, for example,
    //     // getTimeFromMins(1530) to equal getTimeFromMins(90) (i.e. mins rollover)
    //     if (mins >= 24 * 60 || mins < 0) {
    //         throw new RangeError("Valid input should be greater than or equal to 0 and less than 1440.");
    //     }
    //     var h = mins / 60 | 0,
    //         m = mins % 60 | 0;

    //     return moment.utc().hours(h).minutes(m).format("hh:mm") + " H";
    // }

    render() {
        let filas = this.state.recetas.map(el =>
            <tr key={el.id}>
                <td>{el.id}</td>
                <td>{el.nombre}</td>
                <td>{el.descripcion}</td>
                <td>
                    <Button color="danger" onClick={() => this.borra(el.id)}><i className="fa fa-trash-o" aria-hidden="true"></i></Button>
                    {' '}
                    <Link className="btn btn-success" to={"/EditaReceta/" + el.id}><i className="fa fa-pencil-square-o" aria-hidden="true"></i></Link>
                </td>

            </tr>
        );

        return (
            <>
                <h1>Lista de Recetas</h1>
                <br />
                <div className="mb-4">
                    <div className="mb-3">
                        <Label for="nombre">Nombre receta</Label>
                        <Input type="text" className="w-50" id="nombre" name="nombre" onChange={this.handleInputChange} value={this.state.nombre} placeholder="Ej: Macarrones con tomate" />

                        <Label for="descripcion">Descripcion</Label>
                        <Input type="textarea" className="w-50" id="decripcion" name="descripcion" onChange={this.handleInputChange} value={this.state.descripcion} />

                        {/* <Label for="comensales">Comensales</Label>
                        <Input type="number" className="w-50" id="comensales" name="comensales" onChange={this.handleInputChange} value={this.state.comensales} />

                        <Label for="dificultad">Dificultad</Label>
                        <Input type="number" className="w-50" id="dificultad" name="dificultad" onChange={this.handleInputChange} min="0" max="5" value={this.state.dificultad} placeholder="Min 0, Max 5" />

                        <Label for="minutos">Minutos</Label>
                        <Input type="number" className="w-50" id="minutos" name="minutos" onChange={this.handleInputChange} value={this.state.minutos} />*/}
                    </div>
                    <Button disabled={this.state.nombre.length < 2 || this.state.descripcion.length < 5} onClick={this.recetaNueva}>Agregar</Button>
                </div>

                <Table size="sm">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th colSpan="2">Descripcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
            </>
        );
    }
}

export default Recetas;