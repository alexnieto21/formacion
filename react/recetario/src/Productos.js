import React from "react";
import Producto from "./modelos/Producto";
import { Table, Button, Input, Label } from 'reactstrap';

class Productos extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            productos: Producto.getProductos(),
            nombre: '',
            errorNombre: false
        }
        this.productoNuevo = this.productoNuevo.bind(this);
        this.borra = this.borra.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    productoNuevo() {
        if (this.state.nombre.trim() === "") {
            this.setState({ errorNombre: true });
        } else {
            Producto.addProducto(this.state.nombre);
            this.setState({ productos: Producto.getProductos(), errorNombre: false, nombre: '' });
        }

    }

    borra(id) {
        Producto.eliminaProducto(id * 1);
        this.setState({ productos: Producto.getProductos() });
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    render() {

        let filas = this.state.productos.map(el =>
            <tr key={el.id}>
                <td>{el.id}</td>
                <td>{el.nombre}</td>
                <td><Button color="danger" onClick={() => this.borra(el.id)}><i className="fa fa-trash-o" aria-hidden="true"></i></Button></td>
            </tr>
        );

        let errorNombre = <small className="text-danger">El campo no puede ser vacio</small>

        return (
            <>
                <h1>Lista de productos</h1>
                <br />
                <div className="mb-4">
                    <Label for="producto">Nombre producto</Label>
                    <div className="mb-3">
                        <Input type="text" className="w-50" id="producto" name="nombre" onChange={this.handleInputChange} value={this.state.nombre} placeholder="Ej: Pan" />
                        {this.state.errorNombre ? errorNombre : ""}
                    </div>
                    <Button disabled={this.state.nombre.length<2} onClick={this.productoNuevo}>Agregar</Button>
                </div>

                <Table size="sm">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th colSpan="2">Producto</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
            </>
        );
    }
}

export default Productos;
