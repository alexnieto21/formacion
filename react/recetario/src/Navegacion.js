import React from "react";
import { Link } from "react-router-dom";

import { Collapse,
        Navbar,
        NavbarToggler,
        NavbarBrand,
        Nav,
        NavItem,
        NavLink} from 'reactstrap';

class Navegacion extends React.Component {
    
    constructor(props){
        super(props);

        this.state = {
            isOpen: false 
        }

        this.toggle = this.toggle.bind(this);
    }

    toggle(){
        this.setState({isOpen: !this.state.isOpen});
    }

    render() {

        return (
            <>
                <div>
                    <Navbar color="light" light expand="md">
                        <Link to="/" className="navbar-brand">Inicio</Link>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="mr-auto" navbar>
                                <NavItem>
                                    <Link className="nav-link" to="/productos">Productos</Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link" to="/recetas">Recetas</Link>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Navbar>
                </div>
            </>
        );
    }
}

export default Navegacion;