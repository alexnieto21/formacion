import React from 'react';
import Receta from "./modelos/Receta";
import Ingrediente from "./modelos/Ingrediente";
import Producto from "./modelos/Producto";
import { Link, Redirect } from "react-router-dom";
import { Table, Col, Row, Input, Button, Label } from 'reactstrap';

const UNIDADES = [
    'Unidades',
    'Vasos',
    'Tazas',
    'Cucharadas',
    'Litros',
    'ml',
    'Kg',
    'gramos'
];

export default class EditaReceta extends React.Component {

    constructor(props) {
        super(props);

        let receta = Receta.getReceta(this.props.match.params.idReceta * 1);
        let ingredientes = Ingrediente.getIngredientes(receta.id * 1);
        let productos = Producto.getProductos();

        //Si no pones las llaves, se pasa el objeto entero
        //this.state= (receta);

        this.state = {
            id: receta.id,
            nombre: receta.nombre,
            descripcion: receta.descripcion,
            productos: productos,
            ingredientes: ingredientes,
            idIngredienteNuevo: '-1',
            cantidadNueva: '',
            unidadesNueva: UNIDADES[0],
            recetaModificada: false
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.nuevoIngrediente = this.nuevoIngrediente.bind(this);
        this.modificarReceta = this.modificarReceta.bind(this);
        this.volver = this.volver.bind(this);

    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });

        if(name==="nombre" || name==="descripcion"){
            this.setState({recetaModificada: true});
        }

    }

    nuevoIngrediente(){
        let nuevoIngrediente = {
            idReceta: this.state.id,
            idProducto: this.state.idIngredienteNuevo,
            cantidad: this.state.cantidadNueva,
            unidades: this.state.unidadesNueva
        }   

        Ingrediente.addIngrediente(nuevoIngrediente);

        this.setState({ingredientes: Ingrediente.getIngredientes(this.state.id), idIngredienteNuevo: '-1', cantidadNueva: '', unidadesNueva: UNIDADES[0]});
    }

    borraIngrediente(id){
        Ingrediente.eliminaIngrediente(id);
        this.setState({ingredientes: Ingrediente.getIngredientes(this.state.id)});
    }

    modificarReceta(){
        let receta = {
            id: this.state.id,
            nombre: this.state.nombre,
            descripcion: this.state.descripcion
        }

        Receta.modificarReceta(receta);
        this.setState({recetaModificada:false});
    }

    volver(){
        if(this.state.recetaModificada){
            let confirma = confirm("No has guardado las modificaciones, ¿deseas salir de la pagina?");
            if(confirma) this.props.history.push('/recetas');
        }else{
            this.props.history.push('/recetas');
        }
        
    }

    render() {

        let filas = this.state.ingredientes.map(el =>
            <tr key={el.id}>
                <td>{this.state.productos.find(p => p.id === el.producto*1).nombre}</td>
                <td>{el.cantidad + " " + el.unidades}</td>
                <td><Button color="danger" onClick={() => this.borraIngrediente(el.id)}><i className="fa fa-trash-o" aria-hidden="true"></i></Button></td>
            </tr>);

        let opcionesIngredientes = this.state.productos.map(el => <option key={el.id} value={el.id}>{el.nombre}</option>);
        let opcionesUnidades = UNIDADES.map((el,index)=> <option key={index} value={el}>{el}</option>);

        return (
            <>
                <Button color="primary" className="mt-4" size="sm" onClick={this.volver}><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</Button>

                <Input type="text" className="mb-3 mt-4 w-50" style={{fontSize: "22px", fontWeight: "bold"}} id="nombre" name="nombre" onChange={this.handleInputChange} value={this.state.nombre} />
                <Input type="textarea" className="mb-4 mt-4 w-50"  id="descripcion" name="descripcion" onChange={this.handleInputChange} value={this.state.descripcion} />
                <Button disabled={this.state.nombre.length<2 || this.state.descripcion.length<5} onClick={this.modificarReceta}>Guardar</Button>
                <hr/>
                <Row className="mt-4 mb-3">
                    <Col>
                        <h5>Nuevo ingrediente</h5>

                        <Input type="select" className="mb-3" id="ingrediente" name="idIngredienteNuevo" onChange={this.handleInputChange} value={this.state.idIngredienteNuevo} >
                            <option value='-1'>---</option>
                            {opcionesIngredientes}
                        </Input>
                        <Label for="cantidad">Cantidad</Label>
                        <Input type="number" className="mb-3" id="cantidad" name="cantidadNueva" onChange={this.handleInputChange} value={this.state.cantidadNueva} />

                        <Label for="unidades">Unidades</Label>
                        <Input type="select" className="mb-3" id="unidades" name="unidadesNueva" onChange={this.handleInputChange} value={this.state.unidadesNueva} >
                            {opcionesUnidades}
                        </Input>

                        <Button disabled={this.state.idIngredienteNuevo === "-1"} onClick={this.nuevoIngrediente}>Agregar</Button>
                    </Col>
                    <Col>
                        <h5>Ingredientes </h5>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Ingrediente</th>
                                    <th colSpan="2">Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filas}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </>
        );
    }

}