const DATOS = 'recetas';

class Receta {

    static recuperar(){
        return (localStorage.getItem(DATOS)!=null) ? JSON.parse(localStorage.getItem(DATOS)) : [];
    }

    static guardar(lista){
        localStorage.setItem(DATOS, JSON.stringify(lista));
    }

    static getRecetas() {
        return this.recuperar();
    }

    static getReceta(idReceta){
        let recetas = this.recuperar();
        return recetas.find(el=>el.id===idReceta);
    }

    static addReceta({nombre, descripcion}){
        let recetas = this.recuperar();
        let nuevoId = (recetas.length>0)? Math.max(...recetas.map(el=>el.id))+1 : 1;
        let nuevaReceta = {id: nuevoId, nombre: nombre, descripcion: descripcion};
        recetas.push(nuevaReceta);
        this.guardar(recetas);
    }

    static modificarReceta({id, nombre, descripcion}){
        let recetas = this.recuperar();
        recetas.forEach(el=>
            {
                if(el.id===id){
                    el.nombre = nombre;
                    el.descripcion = descripcion;
                }
            }    
        );
        
        this.guardar(recetas);

    }

    static eliminaReceta(idBorrar){
        let recetas = this.recuperar();
        recetas = recetas.filter(el => el.id!==idBorrar);
        this.guardar(recetas);
    }
}

export default Receta;