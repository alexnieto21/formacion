const DATOS = 'productos';

class Producto {

    static recuperar(){
        return (localStorage.getItem(DATOS)!=null) ? JSON.parse(localStorage.getItem(DATOS)) : [];
    }

    static guardar(lista){
        localStorage.setItem(DATOS, JSON.stringify(lista));
    }

    static getProductos() {
        return this.recuperar();
    }

    static getProducto(idProducto){
        let productos = this.recuperar();
        return productos.find(el=>el.id===idProducto);
    }

    static addProducto(nombreProducto){
        let productos = this.recuperar();
        let nuevoId = (productos.length>0)? Math.max(...productos.map(el=>el.id))+1 : 1;
        let productoNuevo = {id: nuevoId, nombre: nombreProducto};
        productos.push(productoNuevo);
        this.guardar(productos);
    }

    static eliminaProducto(idBorrar){
        let productos = this.recuperar();
        productos = productos.filter(el => el.id!==idBorrar);
        this.guardar(productos);
    }
}

export default Producto;