const DATOS = 'ingredientes';

class Ingrediente {

    static recuperar(){
        return (localStorage.getItem(DATOS)!=null) ? JSON.parse(localStorage.getItem(DATOS)) : [];
    }

    static guardar(lista){
        localStorage.setItem(DATOS, JSON.stringify(lista));
    }

    static getIngredientes(idReceta) {
        let ingredientes = this.recuperar();
        return ingredientes.filter(el=>el.receta===idReceta);
    }

    static addIngrediente({idReceta, idProducto, cantidad, unidades}){

        let ingredientes = this.recuperar()

        let nuevoId = (ingredientes.length>0)? Math.max(...ingredientes.map(el=>el.id))+1 : 1;

        let nuevoIngrediente = {
            id: nuevoId,
            producto: idProducto*1,
            receta: idReceta*1,
            cantidad: cantidad*1,
            unidades
        }

        ingredientes.push(nuevoIngrediente);
        this.guardar(ingredientes);
    }

    static eliminaIngrediente(idBorrar){
        let ingredientes = this.recuperar();
        ingredientes = ingredientes.filter(el => el.id!==idBorrar);
        this.guardar(ingredientes);
    }
}

export default Ingrediente;