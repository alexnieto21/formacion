import React from "react";
import { MOTOS } from "./motos.js";
import { Container, Row, Col, Table } from "reactstrap";

class Moto extends React.Component {

    constructor(props) {
        super(props);
    }

    creaFila(moto, indice) {
        return (
            <tr key={indice}>
                <td>{moto.marca}</td>
                <td>{moto.model}</td>
                <td>{moto.eur}€</td>
                <td>{moto.km}</td>
                <td>{moto.cc}</td>
            </tr>
        );
    }

    FilaMotos(moto, indice) {
        return (
            <tr key={indice}>
                <td>{moto.marca}</td>
                <td>{moto.model}</td>
            </tr>
        );
    }

    honda(moto) {
        return moto.marca === "HONDA";
    }

    comparaKm(moto1, moto2) {

        let x = 0;
        if (moto1.km > moto2.km) {
            x = 1;
        } else if (moto1.km < moto2.km) {
            x = -1;
        } else {
            x = 0;
        }

        return x;
    }

    precioDescendente(moto1, moto2) {
        let x = 0;
        if (moto1.eur > moto2.eur) {
            x = -1;
        } else if (moto1.eur < moto2.eur) {
            x = 1;
        }
        return x;
    }

    precioAscendente(moto1, moto2) {
        let x = 0;
        if (moto1.eur > moto2.eur) {
            x = 1;
        } else if (moto1.eur < moto2.eur) {
            x = -1;
        }
        return x;
    }

    motoEspecifica(moto) {
        return moto.km < 25000 && moto.cc > 350 && (moto.eur >= 1800 && moto.eur <= 2200);
    }

    creaFilasMotosMarca(moto, indice) {
        return (
            <tr key={indice} className="text-center">
                <td>{moto}</td>
                <td>{MOTOS.filter(m => m.marca === moto).length}</td>
            </tr>
        );
    }

    render() {

        //let filas = MOTOS.map(this.creaFila);

        //Version: Funcion externa
        //let filas = MOTOS.filter(this.honda).map(this.creaFila);

        //Version: Arrow function
        // let filas = MOTOS
        //     .filter(moto => moto.marca === "YAMAHA")
        //     .sort(this.comparaPrecio)
        //     .map(this.creaFila);

        //let fila = filas[0];
        //let fila10 = motos.slice(0, 10);

        let motosPrecioDescendente = MOTOS.sort(this.precioDescendente);
        let motosCaras = motosPrecioDescendente.filter(el => el.eur === motosPrecioDescendente[0].eur).map(this.creaFila);

        let motosPrecioAscendente = MOTOS.sort(this.precioAscendente);
        let motosBaratas = motosPrecioAscendente.filter(el => el.eur === motosPrecioAscendente[0].eur).map(this.creaFila);

        let motosHonda = MOTOS.filter(el => el.km < 30000).filter(el => el.marca === "HONDA").map(this.creaFila);
        let motos240cc = MOTOS.filter(el => el.km < 30000).filter(el => el.cc > 240).map(this.creaFila);

        let motoEspecifica = MOTOS.filter(this.motoEspecifica).map(this.creaFila);

        let arrayMotos = [];
        MOTOS.forEach(m => arrayMotos.push(m.marca));

        let marcas = [...new Set(arrayMotos)];
        let motos = marcas.map(this.creaFilasMotosMarca);

        return (
            <>
                <Container >
                    <h1 className="text-center mb-4">Motos</h1>
                    <Row>
                        <Col>
                            <Table>
                                <thead>
                                    <tr>
                                        <th colSpan="5" className="text-center">MOTOS MAS CARAS</th>
                                    </tr>
                                    <tr>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Precio</th>
                                        <th>Kilometros</th>
                                        <th>Cilindrada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motosCaras}
                                </tbody>
                            </Table>
                        </Col>
                        <Col className="mt-4 mt-md-0">
                            <Table>
                                <thead>
                                    <tr>
                                        <th colSpan="5" className="text-center">MOTOS MAS BARATAS</th>
                                    </tr>
                                    <tr>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Precio</th>
                                        <th>Kilometros</th>
                                        <th>Cilindrada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motosBaratas}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="mt-4">
                            <Table>
                                <thead>
                                    <tr>
                                        <th colSpan="5" className="text-center">MOTOS HONDA CON MENOS DE 30.000KM</th>
                                    </tr>
                                    <tr>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Precio</th>
                                        <th>Kilometros</th>
                                        <th>Cilindrada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motosHonda}
                                </tbody>
                            </Table>
                        </Col>
                        <Col className="mt-4">
                            <Table>
                                <thead>
                                    <tr>
                                        <th colSpan="5" className="text-center">MOTOS MAS DE 240CC Y MENOS DE 30.000KM</th>
                                    </tr>
                                    <tr>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Precio</th>
                                        <th>Kilometros</th>
                                        <th>Cilindrada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motos240cc}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="mt-4">
                            <Table>
                                <thead>
                                    <tr>
                                        <th colSpan="5" className="text-center">MOTO ESPECIFICA</th>
                                    </tr>
                                    <tr>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Precio</th>
                                        <th>Kilometros</th>
                                        <th>Cilindrada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motoEspecifica}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="mt-4">
                            <Table>
                                <thead>
                                    <tr>
                                        <th colSpan="2" className="text-center">MOTOS POR MARCA</th>
                                    </tr>
                                    <tr className="text-center">
                                        <th>Marca</th>
                                        <th>Numero de motos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {motos}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>

            </>
        );
    }

}

export default Moto;