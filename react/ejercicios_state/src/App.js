import React from "react";
import Thumbs from "./Thumbs";
import Tricolor from "./Tricolor";
import Lista_ciudades from "./Lista_ciudades";
import Moto from "./Moto";
import Combo from "./Combo";
import { CIUTATS_CAT_20K } from "./datos";

export default () => (
  <>
    {/* <Thumbs />
    <Tricolor />
    <Lista_ciudades />
    <Moto /> */}
    <Combo campo1="comarca" campo2="municipi" datos={CIUTATS_CAT_20K}/>
  </>
);
