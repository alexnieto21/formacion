import React from "react";
import "./css/tricolor.css";

class Tricolor extends React.Component{

    constructor(){
        super();

        this.state = {
            color: "gray",
            fondo: 1
        }

        this.cambia = this.cambia.bind(this);
    }

    cambia(){
        
        let colores = ["gray", "red", "green", "blue"];

        if(this.state.fondo===3)this.setState({fondo: 0})
        else this.setState({fondo: this.state.fondo+1})
        
        this.setState({color: colores[this.state.fondo]});

    }

    render(){
        return (
            <>
                <div className="tricolor" style={{backgroundColor: this.state.color}} onClick={this.cambia}></div>
            </>
        );
    }

}

export default Tricolor;