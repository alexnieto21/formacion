import React from "react";
import { CIUTATS_CAT_20K } from "./datos.js";
import { Container, Table } from "reactstrap";

class Lista_ciudades extends React.Component {

    ordenarMunicipi(a,b){ 
        if(a.municipi>b.municipi){
            return 1;
        }else if(a.municipi<b.municipi){
            return -1;
        }else{
            return 0;
        }
    }

    filtrar(num){
        
        let numero = num.poblacio.split('.').join("");
        return numero > 100000;

    }
    
    render() {
        
        let filas = CIUTATS_CAT_20K.filter(this.filtrar);

        filas = filas.sort();
        
        filas = filas.map((el, indice) => <tr key={indice}>
            <td>{el.municipi}</td>
            <td>{el.poblacio}</td>
            <td>{el.provincia}</td>
        </tr>);

        return (
            <>
                <Container className="mt-5">
                    <h1 className="text-center mb-4">Ciutats Catalanes</h1>
                    <Table >
                        <thead>
                            <tr>
                                <th>Municipi</th>
                                <th>Població</th>
                                <th>Provincia</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>
                    </Table>
                </Container>

            </>
        );
    }

}

export default Lista_ciudades;