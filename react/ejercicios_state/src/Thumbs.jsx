import React from "react";

class Thumbs extends React.Component{

    constructor(){
        super();

        this.state = {
            valor: "true"
        }

        this.cambia = this.cambia.bind(this);
    }

    cambia(){
        if(this.state.valor) this.setState({valor:false})
        else this.setState({valor:true}) 
    }

    render(){
        return(
            <>
                <i className={(this.state.valor)?"fa fa-thumbs-up":"fa fa-thumbs-down"} style={{fontSize:"40px"}} onClick={this.cambia}></i>
            </>
        );
    }

}

export default Thumbs;