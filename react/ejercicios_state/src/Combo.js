import React from "react";
import { Container, Form, FormGroup, Label, Input } from "reactstrap";

class Combo extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            campoPrincipal: "",
            campoSecundario: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);

    }

    creaOpcion(opcion, indice){
        return (
            <option key={indice} value={opcion}>{opcion}</option>
        );
    }

    capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
      }

    render() {
        
        let campoPrincipal = this.capitalize(this.props.campo1);
        let campoSecundario = this.capitalize(this.props.campo2);

        let arrayCampoPrincipal = [... new Set(this.props.datos.map(el=>el[this.props.campo1]))];
        let selectPrincipal = arrayCampoPrincipal.sort().map(this.creaOpcion);

        let arrayCampoSecundario = [...new Set(this.props.datos.filter(el=>el[this.props.campo1]===this.state.campoPrincipal).map(el=>el[this.props.campo2]))];
        let selectSecundario = arrayCampoSecundario.sort().map(this.creaOpcion);

        return (
            <>
                <Container>
                    <Form>
                        <FormGroup>
                            <Label for={this.props.campo1}>{campoPrincipal}</Label>
                            <Input type="select" onChange={this.handleInputChange} name="campoPrincipal" id={this.props.campo1}>
                                <option></option>
                                {selectPrincipal}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for={this.props.campo2}>{campoSecundario}</Label>
                            <Input type="select" onClick={this.handleInputChange} name="campoSecundario" id={this.props.campo2}>
                                {selectSecundario}
                            </Input>
                        </FormGroup>
                    </Form>
                    <div>
                        <h3>{this.state.campoSecundario}</h3>
                    </div>
                </Container>
            </>
        );
    }

}

export default Combo;