CREATE DATABASE  IF NOT EXISTS `recetario` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `recetario`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: recetario
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ingredientes`
--

DROP TABLE IF EXISTS `ingredientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productos_id` int(11) NOT NULL,
  `recetas_id` int(11) NOT NULL,
  `cantidad` decimal(8,2) DEFAULT NULL,
  `unidades` varchar(45) DEFAULT NULL,
  `productos_id1` int(11) NOT NULL,
  `recetas_id1` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ingredientes_productos1_idx` (`productos_id`),
  KEY `fk_ingredientes_recetas1_idx` (`recetas_id`),
  KEY `fk_ingredientes_productos1_idx1` (`productos_id1`),
  KEY `fk_ingredientes_recetas1_idx1` (`recetas_id1`),
  CONSTRAINT `fk_ingredientes_productos1` FOREIGN KEY (`productos_id1`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingredientes_recetas1` FOREIGN KEY (`recetas_id1`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredientes`
--

LOCK TABLES `ingredientes` WRITE;
/*!40000 ALTER TABLE `ingredientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_tags_producto`
--

DROP TABLE IF EXISTS `productos_tags_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_tags_producto` (
  `idproductos_tags_producto` int(11) NOT NULL AUTO_INCREMENT,
  `productos_id` int(11) NOT NULL,
  `tags_producto_id` int(11) NOT NULL,
  `productos_id1` int(11) NOT NULL,
  PRIMARY KEY (`idproductos_tags_producto`),
  KEY `fk_productos_tags_producto_productos1_idx` (`productos_id`),
  KEY `fk_productos_tags_producto_tags_producto1_idx` (`tags_producto_id`),
  KEY `fk_productos_tags_producto_productos1_idx1` (`productos_id1`),
  CONSTRAINT `fk_productos_tags_producto_productos1` FOREIGN KEY (`productos_id1`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_tags_producto_tags_producto1` FOREIGN KEY (`tags_producto_id`) REFERENCES `tags_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_tags_producto`
--

LOCK TABLES `productos_tags_producto` WRITE;
/*!40000 ALTER TABLE `productos_tags_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos_tags_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recetas`
--

DROP TABLE IF EXISTS `recetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text,
  `comensales` tinyint(4) DEFAULT NULL,
  `dificultad` tinyint(4) DEFAULT NULL,
  `minutos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recetas`
--

LOCK TABLES `recetas` WRITE;
/*!40000 ALTER TABLE `recetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `recetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recetas_tags_receta`
--

DROP TABLE IF EXISTS `recetas_tags_receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recetas_tags_receta` (
  `idrecetas_tags_receta` int(11) NOT NULL AUTO_INCREMENT,
  `recetas_id` int(11) NOT NULL,
  `tags_receta_id` int(11) NOT NULL,
  `recetas_id1` int(11) NOT NULL,
  PRIMARY KEY (`idrecetas_tags_receta`),
  KEY `fk_recetas_tags_receta_recetas1_idx` (`recetas_id`),
  KEY `fk_recetas_tags_receta_tags_receta1_idx` (`tags_receta_id`),
  KEY `fk_recetas_tags_receta_recetas1_idx1` (`recetas_id1`),
  CONSTRAINT `fk_recetas_tags_receta_recetas1` FOREIGN KEY (`recetas_id1`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_recetas_tags_receta_tags_receta1` FOREIGN KEY (`tags_receta_id`) REFERENCES `tags_receta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recetas_tags_receta`
--

LOCK TABLES `recetas_tags_receta` WRITE;
/*!40000 ALTER TABLE `recetas_tags_receta` DISABLE KEYS */;
/*!40000 ALTER TABLE `recetas_tags_receta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_producto`
--

DROP TABLE IF EXISTS `tags_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_producto`
--

LOCK TABLES `tags_producto` WRITE;
/*!40000 ALTER TABLE `tags_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_receta`
--

DROP TABLE IF EXISTS `tags_receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_receta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_receta`
--

LOCK TABLES `tags_receta` WRITE;
/*!40000 ALTER TABLE `tags_receta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_receta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-21 21:06:30
