let x = 3;
let y = 5;

//1
numeros(x,y);

function numeros(a,b){
  console.log("X="+a+" Y="+b);
}

//2
calcula(x,y);

function calcula(a,b){
  
  let suma = a+b;
  let resta = a-b;
  let multiplicacion = a*b;
  let division = a/b;
  
  console.log("X+Y="+suma);
  console.log("X-Y="+resta);
  console.log("X*Y="+multiplicacion);
  console.log("X/Y="+division);
  
}

//3
let mensaje = xx(x,y);

console.log(mensaje);

function xx(chicos,chicas){
  
  var resultado = (chicas*100)/(chicos+chicas);
  
  return "El porcentaje de chicas es: " + resultado + "%";
  
}

//4
let mensaje = compara(x,y);

console.log(mensaje);

function compara(a,b){
  
  if(a>b){
    return "A es mayor";
  }else if(b>a){
    return "B es mayor";
  }else{
    return "Son iguales";
  }
  
}

//5
let mensaje = mayor(x,y,z);

console.log(mensaje);

function mayor(a,b,c){
  
  if(a>b && a>c){
    return "A es mayor";
  }else if(b>a && b>c){
    return "B es mayor";
  }else{
    return "C es mayor";
  }
  
}

//6
let mensaje = operacion(x,y,z);

console.log(mensaje);

function operacion(a,b,operacion){
  
  if(operacion=="m"){
    var producto = a*b;
    return "El producto es: " + producto;
  }else if(operacion=="s"){
    var suma = a+b;
    return "La suma es: " + suma;
  }else{
    return "Operacion desconocida";
  }
  
}

//7
let mensaje = anyo(a);

console.log(mensaje);

function anyo(num){
  
  if((num%4 === 0 && num !== 0) || num%100 === 0){
    return "Bisiesto";
  }else{
    return "No es bisiesto";
  }
  
}