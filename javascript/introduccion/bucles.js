
function funciones(ej) {

    switch (ej) {
        case 1:
            console.log(ejercicio1());
            break;
        case 2:
            console.log(ejercicio2());
            break;
        case 3:
            console.log(ejercicio3());
            break;
        case 4:
            console.log(ejercicio4());
            break;
        case 5:
            console.log(ejercicio5());
            break;
        case 6:
            console.log(ejercicio6());
            break;
        case 7:
            console.log(ejercicio7());
            break;
        default:
            console.log("No existe ningun ejercicio con ese numero");
    }


}

function ejercicio1() {

    let numero = prompt("Introduce un numero entre 1 y 10");

    while (numero < 1 || numero > 10) {
        console.log("Numero incorrecto");
        numero = prompt("Introduce un numero entre 1 y 10");
    }
    return "El cuadrado de " + numero + " es " + numero * numero;
}

function ejercicio2() {

    let numero, suma = 0;

    do {
        numero = parseInt(prompt("Introduce un numero"));
        suma += numero;
    } while (numero > 0);

    return suma;

}

function ejercicio3() {

    let numero = suma = media = mayor = menor = contador = 0;

    do {
        numero = parseInt(prompt("Introduce un numero"));
        if (numero > 0) {

            if (contador == 0) {
                menor = numero;
                mayor = numero;
            } else {
                if (numero > mayor) {
                    mayor = numero;
                } else if (numero < menor) {
                    menor = numero;
                }
            }

            suma += numero;
            contador++;
        }

    } while (numero > 0);

    if (suma > 0) {
        media = suma / contador;
        return `El  numero mayor es ${mayor}, el menor es ${menor}, la media es ${media}`;
    } else {
        return "Error: debes introducir al menos un numero";
    }

}

function ejercicio4() {

    let numero = parseInt(prompt("Introduce un numero"));
    let factorial = 0;

    if (numero == 0) {
        factorial = 1;
    } else {
        for (let i = 1; i < numero; i++) {

            if (i == 1) {
                factorial = i * parseInt(i + 1);
            } else {
                factorial = factorial * parseInt(i + 1);
            }

        }

    }

    return `El producto factorial de ${numero} es ${factorial}`;

}

function ejercicio5(){

    let numero = parseInt(prompt("Introduce un numero"));
    let n = numero/2;
    let suma = 0;

    for(let i=0;i<n;i++){
        if(i%2==0){
            suma+=i;
        }
    }

    return `La suma de los ${n} primeros numeros es ${suma}`;

}

function ejercicio6(){

    let password = prompt("Introduzca la contraseña:");
    let intentos = 1;

    while(password!="rebeca" && intentos<3){
        intentos++;
        password = prompt("Intente de nuevo:");

    }

    if(password=="rebeca"){
        return `Contraseña correcta`;
    }else{
        return `Acceso denegado`;
    }

}

function ejercicio7(){

    let numero = parseInt(prompt("Introduce un numero"));
    let boom = "";

    for(let i=0;i<numero;i++){

        boom += ` ${parseInt(i+1)}...`;

    }

    boom += "BOOM!";

    return boom;

}


