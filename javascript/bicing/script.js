const url = "https://api.citybik.es/v2/networks/bicing";
const mymap = L.map('mapid').setView([41.3887901, 2.1589899], 15);//L.map('mapid').fitWorld();
//mymap.locate({setView: true, maxZoom: 16});
let marker = new Array();

$(document).ready(function () {
    
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiYWxleG5pZXRvIiwiYSI6ImNrODZiZW11ZTBleDYzbW83ZHgzdmh2eGMifQ.ceoZhnqqG1MNbNn-JSi-oQ'
    }).addTo(mymap);

});

$(document).on("click", "#cargar", cargarEstaciones);


function cargarEstaciones() {

    $.getJSON(url, function (data) {

        $(".tabla>tr").remove();
        removeMarcaEnMapa();

        let estaciones = data.network.stations;
        let numero = $("#numero").val();

        for (let estacion of estaciones) {

            if (estacion.empty_slots >= numero) {
                let data_nombre = estacion.name;
                let data_disponibles = estacion.free_bikes;
                let data_libres = estacion.empty_slots;
                let data_latitud = estacion.latitude;
                let data_longitud = estacion.longitude;

                crearMarcaEnMapa(estacion);

                let fila = $("<tr>");

                let nombre = $("<td>").append(data_nombre);
                let disponibles = $("<td>").append(data_disponibles);
                let libres = $("<td>").append(data_libres);
                let latitud = $("<td>").append(data_latitud);
                let longitud = $("<td>").append(data_longitud);

                fila.append(nombre, disponibles, libres, latitud, longitud);
                $(".tabla").append(fila);

            }

        }

    });

}

function crearMarcaEnMapa(estacion){
    let mark = L.marker([estacion.latitude, estacion.longitude]).addTo(mymap);
    mark.bindPopup(`<b>${estacion.name}</b><br><b>Disponibles: </b>${estacion.free_bikes}<br><b>Libres: </b>${estacion.empty_slots}`);
    marker.push(mark);
}

function removeMarcaEnMapa(){
    for(let m of marker){
        mymap.removeLayer(m);
    }
}
